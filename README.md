# Conway's Game of life

## Introduction

In the year 1970 a mathematician named John Horton Conway invented a logical game during this research. This game he called the **Game of Life**. If you want to read more about the game you can visit [Wikipedia](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) or many other resources on the internet. 

## Concept of the game

- The game is played on a two dimensional **grid** which contains of **cells**. 
- The **width** and the **height** of the grid must be set in the beginning of the game
- Every **cell** has a **state**. The **state** of a cell is either **dead** or **alive**
- In the beginning of the game the player set the **state** of every **cell**.

Actually the **Game of life** is not really a game, as after the start no further interaction between the player and the game is needed. The game start iteration (or let say ***simulating life***) and in every iteration the cells can die, stay alive or be even reborn.

## Rules of the game

Following rules are applied for every new iteration ...

- Any live cell with fewer than two neighbors dies
  - You can call this ***rule of underpopulation***
- Any live cell with two or three neighbors lives on
  - You can call this ***rule of balance***
- Any live cell with more than three live neighbors dies
  - You can call this ***rule of overpopulation***
- Any dead cell with exactly three live neighbors is reborn
  - You can call this ***rule or reproduction***

## Grid layout

The grid use for this game has two dimensions - **x** and **y**. Following graphic shows a 4 x 5 grid

![4x5 grid in the Conway's Game of life](grid.png "4x5 grid in the Conway's Game of life")

## Online version

There are many implementation of this game in the internet. One which run's directly in the browser can be found e.g. [here](https://playgameoflife.com/)