/**
* ATTENTION : Do not touch anything in this file.
*             Feel free to touch every other file in the project!
 */

package main

import (
	"fmt"
	"private/game_of_life/game"
)

func test_game_1() bool {

	// Build up new grid 10x10
	grid := game.NewGrid(10, 10)
	grid.SetAlive(0, 0)
	grid.SetAlive(1, 0)
	grid.SetAlive(0, 1)

	// Create a game engine
	game_engine := game.NewGameEngine(grid)

	// Iteration 0
	game_engine.Print()

	// Go on while there are cells which are alive
	for (game_engine.CountAliveCells() > 0) && (game_engine.GetIteration() < 10) {
		game_engine.NextIteration()
		game_engine.Print()
	}

	// The game should never end and we quit after 10 iterations
	if game_engine.GetIteration() != 10 {
		return false
	}

	// ... and 4 cells are alive
	if game_engine.CountAliveCells() != 4 {
		return false
	}

	return true
}

func test_game_2() bool {
	// Build up new grid 10x10
	grid := game.NewGrid(10, 10)
	grid.SetAlive(1, 0)
	grid.SetAlive(2, 1)
	grid.SetAlive(0, 2)
	grid.SetAlive(1, 2)
	grid.SetAlive(2, 2)

	// Create a game engine
	game_engine := game.NewGameEngine(grid)

	// Iteration 0
	game_engine.Print()

	// Go on while there are cells which are alive
	for (game_engine.CountAliveCells() > 0) && (game_engine.GetIteration() < 33) {
		game_engine.NextIteration()
		game_engine.Print()
	}

	// The game should never end and we quit after 33 iterations
	if game_engine.GetIteration() != 33 {
		return false
	}

	// ... and 4 cells are alive
	if game_engine.CountAliveCells() != 4 {
		return false
	}

	// ... and the 4 cells are the lowest-right in the grid
	var check_cells bool = (game_engine.GetGrid().GetCellState(8, 8) == game.CELL_STATE_ALIVE)
	check_cells = check_cells && (game_engine.GetGrid().GetCellState(8, 9) == game.CELL_STATE_ALIVE)
	check_cells = check_cells && (game_engine.GetGrid().GetCellState(9, 8) == game.CELL_STATE_ALIVE)
	check_cells = check_cells && (game_engine.GetGrid().GetCellState(9, 9) == game.CELL_STATE_ALIVE)
	if !check_cells {
		return false
	}

	return true
}

func test_game_3() bool {
	// Build up new grid 10x10
	grid := game.NewGrid(10, 10)
	grid.SetAlive(2, 1)
	grid.SetAlive(2, 2)
	grid.SetAlive(2, 3)

	// Create a game engine
	game_engine := game.NewGameEngine(grid)

	// Iteration 0
	game_engine.Print()

	// Go on while there are cells which are alive
	for (game_engine.CountAliveCells() > 0) && (game_engine.GetIteration() < 10) {
		game_engine.NextIteration()
		game_engine.Print()
	}

	// The game should never end and we quit after 10 iterations
	if game_engine.GetIteration() != 10 {
		return false
	}

	// ... and 3 cells are alive
	if game_engine.CountAliveCells() != 3 {
		return false
	}

	// ... and this 3 cells are ...
	var check_cells bool = (game_engine.GetGrid().GetCellState(2, 1) == game.CELL_STATE_ALIVE)
	check_cells = check_cells && (game_engine.GetGrid().GetCellState(2, 2) == game.CELL_STATE_ALIVE)
	check_cells = check_cells && (game_engine.GetGrid().GetCellState(2, 3) == game.CELL_STATE_ALIVE)
	if !check_cells {
		return false
	}

	return true
}

func main() {
	fmt.Println("Let's play Conway's Game of Life ... \n")

	fmt.Println("\n[ TEST GAME 1 ] \n")
	if test_game_1() {
		fmt.Println("Test Game 1 ... OK!")
	} else {
		fmt.Println("Test Game 1 ... FAILED!")
	}

	fmt.Println("\n[ TEST GAME 2 ] \n")
	if test_game_2() {
		fmt.Println("Test Game 2 ... OK!")
	} else {
		fmt.Println("Test Game 2 ... FAILED!")
	}

	fmt.Println("\n[ TEST GAME 3 ] \n")
	if test_game_3() {
		fmt.Println("Test Game 3 ... OK!")
	} else {
		fmt.Println("Test Game 3 ... FAILED!")
	}

}
