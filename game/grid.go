package game

import (
	"fmt"
)

type Grid struct {
	height uint
	width uint
	cells []Cell
}

// This function construct a new grid with hight x width cells, which initially are all dead
func NewGrid(height uint, width uint) (this *Grid) {
	return &Grid{height: height, width: width, cells: make([]Cell, height*width)}
}

func (this *Grid) GetHeight() uint {
	return this.height
}

func (this *Grid) GetWidth() uint {
	return this.width
}

// This method makes a copy of the current grid. It may be very usefull to calculate the state
// of the next iteration
func (this *Grid) Copy() *Grid {
	copyGrid := NewGrid(this.height, this.width)
	for i := 0; i < len(this.cells); i++ {
		copyGrid.cells[i] = this.cells[i]
	}
	return copyGrid
}

// This method should print the state of all grid cells.
// For example you can print ...
//
// ----------
// --o-------
// --o-------
// --o-------
// ----------
// ----------
// ----------
// ----------
// ----------
// ----------
//
// For a grid where all cells are dead except (2,1), (2,2) and (2,3)
// You can mark every dead cell with a "0" and every alive cell with an "1"
func (this *Grid) Print() {
	for y := uint(0); y < this.height; y++ {
		for x := uint(0); x < this.width; x++ {
			if this.IsAlive(int(x), int(y)) {
				fmt.Print("1")
			} else {
				fmt.Print("0")
			}
		}
		fmt.Println()
	}
}

// Returns a pointer to a single cell in the game grid
func (this *Grid) GetCellPointer(x int, y int) *Cell {
	index := y * int(this.width) + x
	if index > 0 && index < len(this.cells) {
		return &this.cells[index]
	} else {
		return nil	
	}
}

// Get state of cell at specific location
func (this *Grid) GetCellState(x int, y int) CellState {
	currentCell := this.GetCellPointer(x, y)
	if (currentCell.state == true) {
		return CELL_STATE_ALIVE
	}
	return CELL_STATE_DEAD
}

// Set the state of a cell at specific location
func (this *Grid) SetCellState(x int, y int, state CellState) {
	currentCell := this.GetCellPointer(x, y)
	currentCell.state = state
	return
}

// Returns true if the cell at the location is alive
func (this *Grid) IsAlive(x int, y int) bool {
	currentCell := this.GetCellPointer(x, y)
	if currentCell == nil {
		return false
	} else if currentCell.state == CELL_STATE_ALIVE {
		return true
	}
	return false
}

// Returns true if the cell at the location is dead
func (this *Grid) IsDead(x int, y int) bool {
	return !this.IsAlive(x, y)
}

// Set the cell at the location to the CELL_STATE_ALIVE state
func (this *Grid) SetAlive(x int, y int) {
	currentCell := this.GetCellPointer(x, y)
	if currentCell != nil {
		currentCell.state = CELL_STATE_ALIVE 
	}
}

// Set the cell at the location to the CELL_STATE_DEAD state
func (this *Grid) SetDead(x int, y int) {
	currentCell := this.GetCellPointer(x, y)
	if currentCell != nil {
		currentCell.state = CELL_STATE_DEAD 
	}
}

// Returns the number of cells, which are alive in the grid
func (this *Grid) CountAliveCells() uint {
	result := 0
	for i := 0; i < len(this.cells); i++ {
		if this.cells[i].state == CELL_STATE_ALIVE {
			result++
		}
	}
	return uint(result)
}

// Returns the number of cells in the grid which are dead
func (this *Grid) CountDeadCells() uint {
	result := 0
	result = len(this.cells) - int(this.CountAliveCells())
	return uint(result)
}

// Returns the number of cells in the neighbourhood, which are alive
func (this *Grid) CountAliveNeighbours(x int, y int) uint {
	// Hint : Every cell which is not located at the border of the grid
	//        has 8 neighbors (C is the current cell)
	//
	//        1 2 3
	//        8 C 4
	//        7 6 5
	// 
	//		  (7,3)
	// 	
	// 		  (6,2), (7,2), (8,2), (8,3), (8,4), (7,4), (6,4), (6,3)
	// 
	// 		  (-1,-1), (0,-1), (1,-1), (1,0), (1,1), (0,1), (-1,1), (-1,0)
	//
	//        You can assume neighbors which are not in the grid as dead cells
	var result uint = 0

	for i := -1; i <= 1; i++ {
		for j := -1; j <= 1; j++ {
			if i == 0 && j == 0 {
				continue
			}
			nx := x + i
			ny := y + j

			if this.IsAlive(nx, ny) {
				result++
			}
		}
	}

	return result
}
