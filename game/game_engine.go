package game

import "fmt"

// This is the game engine which will calculate every iteration
type GameEngine struct {
	grid *Grid
	iteration uint
	
}

// This function constructs a new game engine
func NewGameEngine(grid *Grid) *GameEngine {
	return &GameEngine{
		grid:      grid,
		iteration: 0,
	}
}

// This method prints the state of the game engine
func (this *GameEngine) Print() {
	// Hint : Should print the iteration number and the state of the current grid
	fmt.Println("Current iteration: ",this.iteration)
	this.grid.Print()
	fmt.Printf("\n")
}

// This method return the current iteration number of the game (starting with 0)
func (this *GameEngine) GetIteration() uint {
	return this.iteration
}

// This method returns a pointer to the current grid of the game
func (this *GameEngine) GetGrid() *Grid {
	return this.grid
}

// This method returns the number of all cells currently alive in the grid
func (this *GameEngine) CountAliveCells() uint {
	return this.grid.CountAliveCells()
}

// This method returns the number of all cells currently dead in the grid
func (this *GameEngine) CountDeadCells() uint {
	return this.grid.CountDeadCells()
}

// This method returns moves the game engine to the next iteration updating the grid according the game rules
func (this *GameEngine) NextIteration() {
	tempGrid := this.grid.Copy()

	for i := 0; i < int(len(this.grid.cells)); i++ {
		x := i % int(this.grid.width)
		y := i / int(this.grid.width)

		can := this.grid.CountAliveNeighbours(x, y)

		if this.grid.IsAlive(x, y) {
			if can < 2 {							// First rule of underpopulation:
				tempGrid.SetDead(x, y)
			} else if can == 2 || can == 3 {		// Second rule of balance:
				tempGrid.SetAlive(x, y)
			} else {								// Third rule of overpopulation:
				tempGrid.SetDead(x, y)
			}
		} else {
			if can == 3 {							// Fourth rule of overpopulation:
				tempGrid.SetAlive(x, y)
			} else {
				tempGrid.SetDead(x, y)
			}
		}
	}
	this.grid = tempGrid
	this.iteration++
}