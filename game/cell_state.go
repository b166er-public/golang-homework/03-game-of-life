package game

// Every Cell has exactly two possible states - Dead or Alive
type CellState bool

const CELL_STATE_DEAD CellState = false
const CELL_STATE_ALIVE CellState = true
